module.exports = {
    preset: "jest-preset-angular",
    roots: ["<rootDir>/src/"],
    testMatch: ["**/+(*.)+(spec).+(ts)"],
  };
