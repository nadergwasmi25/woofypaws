import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "./header/header.component";
import { TopHeaderComponent } from './top-header/top-header.component';
import { MobileHeaderComponent } from './mobile-header/mobile-header.component';

@NgModule({
    declarations: [HeaderComponent, TopHeaderComponent, MobileHeaderComponent],
    imports: [CommonModule],
    exports: [HeaderComponent]
})
export class CoreModule {}
